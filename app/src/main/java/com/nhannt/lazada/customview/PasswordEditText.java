package com.nhannt.lazada.customview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.nhannt.lazada.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by NhanNT on 04/15/2017.
 */

public class PasswordEditText extends TextInputEditText {

    private Drawable eye, eyeStrike;
    private boolean isVisible = false;
    private boolean isUseStrike = false;
    private boolean isUseValidator = false;
    private Drawable drawable;
    private int ALPHA = (int) (255 * .70f);
    private String MATCHER_PATTERN = "((?=.*\\d)(?=.*[A-Z])(?=.*[a-z]).{6,20})"; //(?=.*\d)
    private Pattern pattern;
    private Matcher matcher;
    private Context mContext;

    public PasswordEditText(Context context) {
        super(context);
        init(null);
        this.mContext = context;
    }

    public PasswordEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
        this.mContext = context;
    }

    public PasswordEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
        this.mContext = context;
    }

    private void init(AttributeSet attrs) {

        this.pattern = Pattern.compile(MATCHER_PATTERN);
        if (attrs != null) {
            TypedArray array = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.PasswordEditText, 0, 0);
            this.isUseStrike = array.getBoolean(R.styleable.PasswordEditText_useStrike, false);
            this.isUseValidator = array.getBoolean(R.styleable.PasswordEditText_useValidator, false);
        }
        eye = ContextCompat.getDrawable(getContext(), R.drawable.ic_visibility_black_24dp).mutate();
        eyeStrike = ContextCompat.getDrawable(getContext(), R.drawable.ic_visibility_off_black_24dp).mutate();
        if (this.isUseValidator) {
            setOnFocusChangeListener(new OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        String text = getText().toString();
                        TextInputLayout textInputLayout = (TextInputLayout) v.getParentForAccessibility();
                        matcher = pattern.matcher(text);
                        if (matcher.matches()) {
                            textInputLayout.setErrorEnabled(false);
                        }else{
                            textInputLayout.setErrorEnabled(true);
                            textInputLayout.setError(mContext.getString(R.string.password_require));
                        }
                    }
                }
            });
        }
        setUp();
    }

    private void setUp() {
        setInputType(InputType.TYPE_CLASS_TEXT |
                (isVisible ? InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD : InputType.TYPE_TEXT_VARIATION_PASSWORD));
        Drawable[] drawables = getCompoundDrawables();
        drawable = isUseStrike && !isVisible ? eyeStrike : eye;
        drawable.setAlpha(ALPHA);
        setCompoundDrawablesWithIntrinsicBounds(drawables[0], drawables[1], drawable, drawables[3]);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_UP && event.getX() >= (getRight() - drawable.getBounds().width())) {
            isVisible = !isVisible;
            setUp();
            invalidate();
        }

        return super.onTouchEvent(event);
    }
}
