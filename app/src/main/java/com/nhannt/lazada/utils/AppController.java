package com.nhannt.lazada.utils;

import android.app.Application;
import android.content.Context;

/**
 * Created by NhanNT on 04/16/2017.
 */

public class AppController extends Application {
    private static AppController mInstance;


    public static Context getContext() {
        return mInstance.getApplicationContext();
    }

    public static AppController getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

}
