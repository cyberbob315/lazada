package com.nhannt.lazada.view.home;

import android.content.Context;

import com.nhannt.lazada.model.objectclass.ProductType;

import java.util.List;

/**
 * Created by NhanNT on 04/15/2017.
 */

public interface IViewHandleMenu {
    void showListMenuItem(List<ProductType> listProduct);
    void loginAccountExist(String username);
    void noCurrentLoginAcc();
    Context getViewContext();

}
