package com.nhannt.lazada.view.login.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.nhannt.lazada.R;
import com.nhannt.lazada.presenter.login.IPresenterLogin;
import com.nhannt.lazada.presenter.login.PresenterLogin;
import com.nhannt.lazada.utils.AppController;
import com.nhannt.lazada.view.home.HomeActivity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentLogin extends Fragment implements View.OnClickListener,GoogleApiClient.OnConnectionFailedListener,IViewLogin {

    @BindView(R.id.btn_login_facebook)
    protected Button btnLoginFacebook;
    @BindView(R.id.btn_login_google)
    protected Button btnLoginGoogle;

    private CallbackManager callbackManager;
    private IPresenterLogin presenterLogin;
    private static final int RC_SIGN_IN = 9001;
    private ProgressDialog mProgressDialog;
    private Context mContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        initFBLogin();

        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        presenterLogin = new PresenterLogin(this);
        btnLoginFacebook.setOnClickListener(this);
        btnLoginGoogle.setOnClickListener(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    private void initFBLogin() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d("Check","Login facebook success");
                navigateToHomeActivity();
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException error) {
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RC_SIGN_IN){
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if(result.isSuccess()){
                Log.d("Check","Login google success");
                presenterLogin.receiveGoogleLoginResult(result);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_login_facebook:
                LoginManager.getInstance().logInWithReadPermissions(FragmentLogin.this,
                        Arrays.asList("public_profile","email"));
                break;
            case R.id.btn_login_google:
                presenterLogin.loginGooglePlus(mContext);
                break;
        }
    }


    @Override
    public void onPause() {
        super.onPause();
        presenterLogin.onPause(mContext);

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void startGoogleLoginIntent(GoogleApiClient googleApiClient) {
        Intent iGooglePlus = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
        startActivityForResult(iGooglePlus, RC_SIGN_IN);
    }

    @Override
    public void loginGoogleSuccess(GoogleSignInResult result) {
        Toast.makeText(AppController.getContext(), "Login success", Toast.LENGTH_SHORT).show();
        navigateToHomeActivity();
    }

    @Override
    public void loginGoogleFail() {

    }

    @Override
    public void connectionFail(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public Context getViewContext() {
        return getContext();
    }

    @Override
    public void navigateToHomeActivity() {
        Intent iHomePage = new Intent(getActivity(), HomeActivity.class);
        startActivity(iHomePage);
    }

    @Override
    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenterLogin.onDestroy();
        presenterLogin = null;
    }
}
