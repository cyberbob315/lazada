package com.nhannt.lazada.view.login.fragment;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by NhanNT on 04/16/2017.
 */

public interface IViewLogin {
    void loginGoogleSuccess(GoogleSignInResult result);
    void loginGoogleFail();
    void connectionFail(@NonNull ConnectionResult connectionResult);
    void startGoogleLoginIntent(GoogleApiClient googleApiClient);
    Context getViewContext();
    void navigateToHomeActivity();
    void showProgressDialog();
    void hideProgressDialog();
}
