package com.nhannt.lazada.view.home;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;

import com.nhannt.lazada.R;
import com.nhannt.lazada.adapter.ExpandAdapter;
import com.nhannt.lazada.adapter.HomeViewPagerAdapter;
import com.nhannt.lazada.model.login.IModelLogin;
import com.nhannt.lazada.model.objectclass.ProductType;
import com.nhannt.lazada.presenter.home.handledrawermenu.PresenterHandleMenu;
import com.nhannt.lazada.view.login.ActivityLogin;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity implements IViewHandleMenu, AppBarLayout.OnOffsetChangedListener {
    @BindView(R.id.toolbar)
    protected Toolbar mToolbar;
    @BindView(R.id.viewpager)
    protected ViewPager mViewPager;
    @BindView(R.id.tabs)
    protected TabLayout mTabLayout;
    @BindView(R.id.drawer_layout)
    protected DrawerLayout mDrawerLayout;
    @BindView(R.id.ep_lv_menu)
    protected ExpandableListView mExpandableListView;
    @BindView(R.id.appbar_home)
    protected AppBarLayout mAppBar;
    @BindView(R.id.collapse_toolbar_home)
    protected CollapsingToolbarLayout mCollapsingToolbar;
    @BindView(R.id.ll_search)
    protected LinearLayout searchBar;

    private ActionBarDrawerToggle mDrawerToggle;
    private PresenterHandleMenu presenterHandleMenu;
    private String username = "";
    private Menu menu;
    private MenuItem menuItemLogin;
    private MenuItem menuItemLogout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenterHandleMenu = new PresenterHandleMenu(this);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        setupToolbar();
        setupViewPagerTabLayout();
        setupDrawerLayout();
        mAppBar.addOnOffsetChangedListener(this);
        presenterHandleMenu.getMenuItemList();

    }

    private void setupDrawerLayout() {
        mDrawerToggle = new ActionBarDrawerToggle(HomeActivity.this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close);
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mDrawerToggle.syncState();
    }

    private void setupViewPagerTabLayout() {
        HomeViewPagerAdapter adapter = new HomeViewPagerAdapter(getSupportFragmentManager(), HomeActivity.this);
        mViewPager.setAdapter(adapter);
        mTabLayout.setupWithViewPager(mViewPager);
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("");
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_home, menu);

        menuItemLogin = menu.findItem(R.id.item_login);
        menuItemLogout = menu.findItem(R.id.item_logout);
        presenterHandleMenu.checkCurrentLoginUser();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        int id = item.getItemId();
        switch (id) {
            case R.id.item_login:
                if (presenterHandleMenu.getLoginType() == IModelLogin.LoginType.Visitor) {
                    Intent iLogin = new Intent(HomeActivity.this, ActivityLogin.class);
                    startActivity(iLogin);
                    Log.d("Check", "go to login");
                }
                break;
            case R.id.item_logout:
                presenterHandleMenu.logoutAccount();
                this.menu.clear();
                this.onCreateOptionsMenu(this.menu);
                break;
        }
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenterHandleMenu.onPause();
    }

    @Override
    public void showListMenuItem(List<ProductType> listProduct) {
        ExpandAdapter expandAdapter = new ExpandAdapter(HomeActivity.this, listProduct);
        mExpandableListView.setAdapter(expandAdapter);
        expandAdapter.notifyDataSetChanged();
    }

    @Override
    public void loginAccountExist(String username) {
        this.username = username;
        menuItemLogout.setVisible(true);
        menuItemLogin.setTitle(getString(R.string.hello) + " " + username);
    }

    @Override
    public void noCurrentLoginAcc() {
        this.username = null;
    }

    @Override
    public Context getViewContext() {
        return this;
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        if (mCollapsingToolbar.getHeight() + verticalOffset <= 1.5 * ViewCompat.getMinimumHeight(mCollapsingToolbar)) {
            searchBar.animate().alpha(0).setDuration(500);
        } else {
            searchBar.animate().alpha(1).setDuration(500);
        }
    }
}
