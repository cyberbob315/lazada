package com.nhannt.lazada.view.login;

import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.facebook.FacebookSdk;
import com.nhannt.lazada.R;
import com.nhannt.lazada.adapter.ViewPagerAdapterLogin;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityLogin extends AppCompatActivity {
    @BindView(R.id.toolbar_login)
    protected Toolbar mToolbar;
    @BindView(R.id.tab_login)
    protected TabLayout tabLayout;
    @BindView(R.id.viewpager_login)
    protected ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        setupToolbar();
        setupViewPager();
    }

    private void setupToolbar() {
        mToolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.colorWhite));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.login_and_register));
    }

    private void setupViewPager() {
        ViewPagerAdapterLogin pagerAdapter = new ViewPagerAdapterLogin(getSupportFragmentManager(), ActivityLogin.this);
        viewPager.setAdapter(pagerAdapter);
        pagerAdapter.notifyDataSetChanged();
        tabLayout.setupWithViewPager(viewPager);
    }
}
