package com.nhannt.lazada.view.splash;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.nhannt.lazada.R;
import com.nhannt.lazada.view.home.HomeActivity;

public class ActivitySplash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    Thread.sleep(2000);
                }catch (Exception ex){

                }finally {
                    Intent iHome = new Intent(ActivitySplash.this, HomeActivity.class);
                    startActivity(iHome);
                    finish();
                }
            }
        });
        thread.start();
    }
}
