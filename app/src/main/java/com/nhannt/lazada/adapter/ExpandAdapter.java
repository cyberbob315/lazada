package com.nhannt.lazada.adapter;

import android.content.Context;
import android.graphics.Point;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nhannt.lazada.R;
import com.nhannt.lazada.model.home.handledrawermenu.HandleHomeMenu;
import com.nhannt.lazada.model.home.handledrawermenu.IHandleHomeMenu;
import com.nhannt.lazada.model.objectclass.ProductType;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by NhanNT on 04/15/2017.
 */

public class ExpandAdapter extends BaseExpandableListAdapter {

    private Context mContext;
    private List<ProductType> listProduct;
    private LayoutInflater mLayoutInflater;
    ViewHolderMenu viewHolderMenu;

    public ExpandAdapter(Context context, List<ProductType> data) {
        this.mContext = context;
        this.listProduct = data;
        mLayoutInflater = LayoutInflater.from(context);
        IHandleHomeMenu handleJSON = new HandleHomeMenu();
        int count = data.size();
        for (int i = 0; i < count; i++) {
            int productCode = listProduct.get(i).getProductCode();
            listProduct.get(i).setListChildProduct(handleJSON.getProductListByParentCode(productCode));
        }

    }

    @Override
    public int getGroupCount() {
        return listProduct.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (listProduct.get(groupPosition).getListChildProduct().size() != 0) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public Object getGroup(int groupPosition) {
        return listProduct.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return listProduct.get(groupPosition).getListChildProduct().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return listProduct.get(groupPosition).getProductCode();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return listProduct.get(groupPosition).getListChildProduct().get(childPosition).getProductCode();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.custom_layout_expandable_group, parent, false);
            viewHolderMenu = new ViewHolderMenu(convertView);
            convertView.setTag(viewHolderMenu);
        } else {
            viewHolderMenu = (ViewHolderMenu) convertView.getTag();
        }
        viewHolderMenu.tvTitle.setText(listProduct.get(groupPosition).getProductName());

        int countChildProduct = listProduct.get(groupPosition).getListChildProduct().size();
        if (countChildProduct > 0) {
            viewHolderMenu.btExpand.setVisibility(View.VISIBLE);
        } else {
            viewHolderMenu.btExpand.setVisibility(View.INVISIBLE);
        }
        if (isExpanded) {
            viewHolderMenu.btExpand.setImageResource(R.drawable.ic_remove_black_24dp);
            if (countChildProduct > 0)
                convertView.setBackgroundResource(R.color.colorGray);
        } else {
            viewHolderMenu.btExpand.setImageResource(R.drawable.ic_add_black_24dp);
            convertView.setBackgroundResource(R.color.colorWhite);
        }

        convertView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Toast.makeText(mContext, "Ma loai sp :" + listProduct.get(groupPosition).getProductCode()
                        + " " + listProduct.get(groupPosition).getProductName(), Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        CustomExpandableListView secondExpandable = new CustomExpandableListView(mContext);
        ExpandAdapter secondAdapter = new ExpandAdapter(mContext, listProduct.get(groupPosition).getListChildProduct());
        secondExpandable.setAdapter(secondAdapter);

        secondExpandable.setGroupIndicator(null);
        notifyDataSetChanged();
        return secondExpandable;
    }

    public class CustomExpandableListView extends ExpandableListView {

        public CustomExpandableListView(Context context) {
            super(context);
        }

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            WindowManager windowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
            Display display = windowManager.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);

            int width = size.x;
            int height = size.y;

//            widthMeasureSpec = MeasureSpec.makeMeasureSpec(width, MeasureSpec.AT_MOST);
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.AT_MOST);

            Log.d("size", width + " - " + height);
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }


    public class ViewHolderMenu {
        @BindView(R.id.tv_product_name_group)
        TextView tvTitle;
        @BindView(R.id.iv_expand_menu)
        ImageView btExpand;

        public ViewHolderMenu(View itemView){
            ButterKnife.bind(this,itemView);
        }
    }

}
