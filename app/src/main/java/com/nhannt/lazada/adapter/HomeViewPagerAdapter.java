package com.nhannt.lazada.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;

import com.nhannt.lazada.R;
import com.nhannt.lazada.view.home.fragment.FragmentBrand;
import com.nhannt.lazada.view.home.fragment.FragmentElectronic;
import com.nhannt.lazada.view.home.fragment.FragmentFashion;
import com.nhannt.lazada.view.home.fragment.FragmentHealthBeauty;
import com.nhannt.lazada.view.home.fragment.FragmentHighlights;
import com.nhannt.lazada.view.home.fragment.FragmentHomeLiving;
import com.nhannt.lazada.view.home.fragment.FragmentMotherBaby;
import com.nhannt.lazada.view.home.fragment.FragmentPromotion;
import com.nhannt.lazada.view.home.fragment.FragmentSportTravel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NhanNT on 04/14/2017.
 */

public class HomeViewPagerAdapter extends FragmentPagerAdapter {

    private Context mContext;
    private List<Fragment> listFragment = new ArrayList<Fragment>();
    private List<String> titleFragment = new ArrayList<String>();

    public HomeViewPagerAdapter(FragmentManager fm, Context mContext) {
        super(fm);
        this.mContext = mContext;

        listFragment.add(new FragmentHighlights());
        listFragment.add(new FragmentPromotion());
        listFragment.add(new FragmentElectronic());
        listFragment.add(new FragmentHomeLiving());
        listFragment.add(new FragmentMotherBaby());
        listFragment.add(new FragmentHealthBeauty());
        listFragment.add(new FragmentFashion());
        listFragment.add(new FragmentSportTravel());
        listFragment.add(new FragmentBrand());

        titleFragment.add(mContext.getString(R.string.highlights));
        titleFragment.add(mContext.getString(R.string.promotion));
        titleFragment.add(mContext.getString(R.string.electronic));
        titleFragment.add(mContext.getString(R.string.home_living));
        titleFragment.add(mContext.getString(R.string.mother_baby));
        titleFragment.add(mContext.getString(R.string.health_beauty));
        titleFragment.add(mContext.getString(R.string.fashion));
        titleFragment.add(mContext.getString(R.string.sport_travel));
        titleFragment.add(mContext.getString(R.string.brand));
    }

    @Override
    public Fragment getItem(int position) {
        return listFragment.get(position);
    }

    @Override
    public int getCount() {
        return listFragment.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titleFragment.get(position);
    }
}
