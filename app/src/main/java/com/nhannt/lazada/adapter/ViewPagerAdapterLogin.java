package com.nhannt.lazada.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.nhannt.lazada.R;
import com.nhannt.lazada.view.login.fragment.FragmentLogin;
import com.nhannt.lazada.view.login.fragment.FragmentRegister;

/**
 * Created by NhanNT on 04/15/2017.
 */

public class ViewPagerAdapterLogin extends FragmentPagerAdapter {
    private Context mContext;

    public ViewPagerAdapterLogin(FragmentManager fm, Context context) {
        super(fm);
        this.mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                FragmentLogin fragmentLogin = new FragmentLogin();
                return fragmentLogin;
            case 1:
                FragmentRegister fragmentRegister = new FragmentRegister();
                return fragmentRegister;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return mContext.getString(R.string.login);
            case 1:
                return mContext.getString(R.string.register);
            default:
                return null;
        }
    }
}
