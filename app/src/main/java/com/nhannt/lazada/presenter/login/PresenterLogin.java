package com.nhannt.lazada.presenter.login;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.nhannt.lazada.model.login.IModelLogin;
import com.nhannt.lazada.model.login.ModelLogin;
import com.nhannt.lazada.view.login.fragment.IViewLogin;


/**
 * Created by NhanNT on 04/16/2017.
 */

public class PresenterLogin implements IPresenterLogin {

    private IViewLogin mView;
    private IModelLogin mModel;
    private GoogleApiClient mGoogleApiClient;

    public PresenterLogin(IViewLogin mView) {
        this.mView = mView;
        this.mModel = new ModelLogin();
    }

    @Override
    public void loginGooglePlus(Context context) {
        mView.showProgressDialog();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        } else {
            mGoogleApiClient = mModel.getGoogleApiClient(context, this);
        }
        mView.startGoogleLoginIntent(mGoogleApiClient);
    }


    @Override
    public void receiveGoogleLoginResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            mView.loginGoogleSuccess(result);
        } else {
            mView.loginGoogleFail();
        }
        mView.hideProgressDialog();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        mView.connectionFail(connectionResult);
    }

    @Override
    public void onPause(Context context) {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage((AppCompatActivity) context);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onDestroy() {
        this.mView = null;
        this.mModel = null;
    }
}
