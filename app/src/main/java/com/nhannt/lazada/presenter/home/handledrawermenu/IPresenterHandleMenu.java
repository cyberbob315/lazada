package com.nhannt.lazada.presenter.home.handledrawermenu;

import com.facebook.AccessToken;
import com.nhannt.lazada.model.login.IModelLogin;

/**
 * Created by NhanNT on 04/15/2017.
 */

public interface IPresenterHandleMenu {
    void getMenuItemList();
    AccessToken getUserNameFacebook();
    void checkCurrentLoginUser();
    void logoutAccount();
    IModelLogin.LoginType getLoginType();
    void onPause();
}
