package com.nhannt.lazada.presenter.login;

import android.content.Context;

import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by NhanNT on 04/16/2017.
 */

public interface IPresenterLogin extends GoogleApiClient.OnConnectionFailedListener {
    void loginGooglePlus(Context context);
    void receiveGoogleLoginResult(GoogleSignInResult result);
    void onDestroy();
    void onPause(Context context);
}
