package com.nhannt.lazada.presenter.home.handledrawermenu;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.nhannt.lazada.model.home.handledrawermenu.HandleHomeMenu;
import com.nhannt.lazada.model.home.handledrawermenu.IHandleHomeMenu;
import com.nhannt.lazada.model.login.IModelLogin;
import com.nhannt.lazada.model.login.ModelLogin;
import com.nhannt.lazada.model.objectclass.ProductType;
import com.nhannt.lazada.network.DownloadJSON;
import com.nhannt.lazada.utils.Constants;
import com.nhannt.lazada.view.home.IViewHandleMenu;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by NhanNT on 04/15/2017.
 */

public class PresenterHandleMenu implements IPresenterHandleMenu, GraphRequest.GraphJSONObjectCallback, GoogleApiClient.OnConnectionFailedListener {
    private IViewHandleMenu mView;
    private IModelLogin modelLogin;
    private String username = "";
    private static IModelLogin.LoginType loginType;
    private GoogleApiClient googleApiClient;
    private AccessToken accessToken;

    public PresenterHandleMenu(IViewHandleMenu view) {
        this.mView = view;
        this.modelLogin = new ModelLogin();
        this.loginType = IModelLogin.LoginType.Visitor;
    }

    @Override
    public void getMenuItemList() {
        List<ProductType> listProduct;
        String dataJSON = "";
        List<HashMap<String, String>> attrs = new ArrayList<>();

        //Get data by GET method
//        String urlGET = "http://10.0.3.2:8080/weblazada/loaisanpham.php?maloaicha=0";
//        DownloadJSON downloadJSON = new DownloadJSON(urlGET);
        //End GET method
        //Get data by POST method
        String urlPOST = Constants.BASE_URL + "loaisanpham.php";
        HashMap<String, String> hsParentCode = new HashMap<>();
        hsParentCode.put("maloaicha", "0");
        attrs.add(hsParentCode);
        DownloadJSON downloadJSON = new DownloadJSON(urlPOST, attrs);
        //End POST method
        downloadJSON.execute();

        try {
            dataJSON = downloadJSON.get();
            IHandleHomeMenu handleJSOn = new HandleHomeMenu();
            listProduct = handleJSOn.parseJSONMenu(dataJSON);
            mView.showListMenuItem(listProduct);

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } finally {
        }
    }

    @Override
    public AccessToken getUserNameFacebook() {
        accessToken = modelLogin.getCurrentAccessTokenFacebook();
        return accessToken;
    }

    @Override
    public void checkCurrentLoginUser() {
        //Login facebook
        accessToken = modelLogin.getCurrentAccessTokenFacebook();
        if (accessToken != null) {
            GraphRequest graphRequest = GraphRequest.newMeRequest(accessToken, this);
            Bundle params = new Bundle();
            params.putString("fields", "name");
            graphRequest.setParameters(params);
            graphRequest.executeAsync();
        }
        //Login google
        if(googleApiClient != null){
            googleApiClient.connect();
        }else {
            googleApiClient = modelLogin.getGoogleApiClient(mView.getViewContext(), this);
        }
        GoogleSignInResult googleSignInResult = modelLogin.getLoginInfoGoogle(googleApiClient);
        if (googleSignInResult != null && googleSignInResult.isSuccess()) {
            loginType = IModelLogin.LoginType.GoogleAccount;
            mView.loginAccountExist(googleSignInResult.getSignInAccount().getDisplayName());
            Log.d("LoginGoogle", googleSignInResult.getSignInAccount().getDisplayName());
        }
    }

    @Override
    public void logoutAccount() {
        if (loginType == IModelLogin.LoginType.FacebookAccount)
            logoutFacebook();
        else if (loginType == IModelLogin.LoginType.GoogleAccount)
            logoutGoogle();
        else if (loginType == IModelLogin.LoginType.LazadaAccount)
            logoutLazadaAccount();
        loginType = IModelLogin.LoginType.Visitor;

    }

    private void logoutLazadaAccount() {

    }

    private void logoutFacebook() {
        LoginManager.getInstance().logOut();
    }

    private void logoutGoogle() {
        Auth.GoogleSignInApi.signOut(googleApiClient);
    }

    @Override
    public void onPause() {
        if (googleApiClient != null&& googleApiClient.isConnected() ) {
            googleApiClient.stopAutoManage((AppCompatActivity) mView.getViewContext());
            googleApiClient.disconnect();
        }
    }

    @Override
    public IModelLogin.LoginType getLoginType() {
        return this.loginType;
    }

    @Override
    public void onCompleted(JSONObject object, GraphResponse response) {
        try {
            username = object.getString("name");
            loginType = IModelLogin.LoginType.FacebookAccount;
            mView.loginAccountExist(username);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
