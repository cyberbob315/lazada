package com.nhannt.lazada.model.home.handledrawermenu;

import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.nhannt.lazada.model.objectclass.ProductType;
import com.nhannt.lazada.network.DownloadJSON;
import com.nhannt.lazada.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by NhanNT on 04/15/2017.
 */

public class HandleHomeMenu implements IHandleHomeMenu {



    @Override
    public List<ProductType> parseJSONMenu(String jsonData) {
        List<ProductType> listProduct = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(jsonData);
            JSONArray productList = jsonObject.getJSONArray("LOAISANPHAM");
            int count = productList.length();
            for (int i = 0; i < count; i++) {
                JSONObject values = productList.getJSONObject(i);

                ProductType dataProduct = new ProductType();
                dataProduct.setProductCode(Integer.parseInt(values.getString("MALOAISP")));
                dataProduct.setParentCode(Integer.parseInt(values.getString("MALOAI_CHA")));
                dataProduct.setProductName(values.getString("TENLOAISP"));

                listProduct.add(dataProduct);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return listProduct;
    }

    @Override
    public List<ProductType> getProductListByParentCode(int parentCode) {
        List<ProductType> listProduct = new ArrayList<>();
        List<HashMap<String, String>> attrs = new ArrayList<>();
        String dataJSON = "";

        String urlPOST = Constants.BASE_URL + "loaisanpham.php";
        HashMap<String, String> hsParentCode = new HashMap<>();
        hsParentCode.put("maloaicha", parentCode + "");
        attrs.add(hsParentCode);
        DownloadJSON downloadJSON = new DownloadJSON(urlPOST, attrs);

        downloadJSON.execute();

        try {
            dataJSON = downloadJSON.get();
            HandleHomeMenu handleJSOn = new HandleHomeMenu();
            listProduct = handleJSOn.parseJSONMenu(dataJSON);

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } finally {
        }

        return listProduct;
    }




}
