package com.nhannt.lazada.model.objectclass;

import java.util.List;

/**
 * Created by NhanNT on 04/15/2017.
 */

public class ProductType {
    private int ProductCode, ParentCode;
    private String ProductName;
    private List<ProductType> listChildProduct;

    public int getProductCode() {
        return ProductCode;
    }

    public void setProductCode(int productCode) {
        ProductCode = productCode;
    }

    public int getParentCode() {
        return ParentCode;
    }

    public void setParentCode(int parentCode) {
        ParentCode = parentCode;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public List<ProductType> getListChildProduct() {
        return listChildProduct;
    }

    public void setListChildProduct(List<ProductType> listChildProduct) {
        this.listChildProduct = listChildProduct;
    }
}
