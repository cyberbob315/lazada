package com.nhannt.lazada.model.home.handledrawermenu;

import com.facebook.AccessToken;
import com.nhannt.lazada.model.objectclass.ProductType;

import java.util.List;

/**
 * Created by NhanNT on 04/16/2017.
 */

public interface IHandleHomeMenu {
    List<ProductType> parseJSONMenu(String jsonData);
    List<ProductType> getProductListByParentCode(int parentCode);
}
