package com.nhannt.lazada.model.login;

import android.content.Context;

import com.facebook.AccessToken;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by NhanNT on 04/16/2017.
 */

public interface IModelLogin {
    public enum LoginType{
        LazadaAccount,FacebookAccount,GoogleAccount,Visitor
    }
    AccessToken getCurrentAccessTokenFacebook();

    void cancelTokenTracker();

    GoogleApiClient getGoogleApiClient(Context context, GoogleApiClient.OnConnectionFailedListener failedListener);

    GoogleSignInResult getLoginInfoGoogle(GoogleApiClient googleApiClient);
}
